
# EDOM - Project Research Topic

**Topic**: Eclipse Papyrus

## Índice
1. Introdução do Papyrus;
	 1.1. Apresentação da tecnologia/ferramenta;
2. Model Driven Engineering, utilizando Papyrus;
	 2.1 Model driven Engineering - Como foi criado o  Papyrus ?
		 2.2.1 Application  engineering - Papyrus for Robotics
3. Exemplo de utilização de Papyrus;
	3.1 Utilização do Papyrus - Papyrus for Robotics
4. Bibliografia.

---

## 1. Introdução do Papyrus
O papyrus foi definida pela OMG (Object Management Group) como uma ferramenta de edição gráfica para UML2. Este tem como objetivos implementar a totalidade das especificações da OMG. O papyrus fornece tambem suporte para SysML de forma a permitir sistemas baseados em modelos. Assim, o papyrus para além de fornecer um editor para diagramas UML, fornece também editores para SysML.

### 1.1. Apresentação da tecnologia/ferramenta
Tal como previamente, Papyrus UML é uma ferramenta de edição grafica, open-source, fornece suporte para UML 2, SysML entre outras.
Um projeto em Papyrus pode ter tantos modelos como diagramas podem ser adicionados podem ser adicionados ao modelo. 
	
## 2. Model Driven Engineering - O que é ?

A Model Driven Engineering (engenharia orientada a modelos) tem como intuito definir modelos, métodos e ferramentas adequados para a representação precisa e eficiente de sistemas intensivos de software. O Model Driven Engineering (MDE) visa abranger todo o ciclo de vida de um sistema, de acordo com várias dimensões, como requisitos, funcionalidades, dados, processamento, dependências, arquitectura e infraestrutura do sistema. Modelos, métodos e ferramentas juntos visam apoiar os engenheiros de software e outros partes interessadas do sistema durante todo o ciclo de vida, desde o levantamento de requisitos até a implementação.
A ONG, como parte da sua iniciativa de MDE, providenciou uma serie de recomendações de tecnologia standarizada como suporte do desenvolvimento baseado em modelos. Esta recomendações abragem os principais recursos, como metamodelos, transformações de modelo e linguagens domain-specific. Um componente chave nesta ultima categoria é a UML (Unified Modeling Language), que é a mais usada, tanto no meio académico como empresarial. 


### 2.1 Model driven Engineering - Como foi criado o  Papyrus ?

Existe uma grande variedade de ferramentas que suportam o UML, no entanto estas são geralmente vendidas pelos seus respectivos proprietários, tornando-se problemático pelo ponto de vista dos utilizadores - que podem necessitar de um alto nivel de suporte, o que , tendo em conta a perspectiva do vendedor, nem sempre o inclui, ou está pensado. Assim, consequentemente, algumas empresas procuraram soluções open-source - são obviamente mais baratas, o que acaba por se tornar um grande fator de decisao, embora este não seja o principal, visto que existe um custo elevado na integração destas soluções. 
Semelhante ao que é encontrado na pesquisa/ investigação, que normalmente depende de ferramentas open -source, visto que as restantes são muito limitadas e inflexiveis, de forma a permitir a otimização da implementação de novas ideias e protótipos. 
Assim, o Eclipse, juntamente com as suas *model development tools* é o ambiente de escolha para o desenvolvimento de ferramentas open-source para modelos. 
Com este objectivo em mente, o MDT (model development tools) pretende: 

 - Implementação dos metamodelos da industria; 
 - Ferramentas para desenvolver modelos baseados nesses metamodelos. 
 
 O UML2 foi assim o seu primeiro componente. Baseado neste surgiram então series de ferramentas open-source, providenciando facilidade para a modelagem gráfica, dentro do Eclipse, tais como : MOSKitt, Papyrus e TOPCASED, que no inicio de 2008 se fundiram e criaram um novo editor grafico, denomeado como Papyrus. Este foi aceite pelo Eclipse's Project Management Commitee. 
 
### 2.2. Arquitetura do Papyrus

 Papyrus é integrado no Eclipse como um editor ligado ao modelo UML 2, este gere os múltiplos diagramas ao invés do Eclipse. O papyrus é altamente personalizavel e permita adicionar novos tipos de diagramas desenvolvidos usando qualquer tecnologia compativel com o Eclipse (GEF, GMF, EMF Tree Editors, etc). Isto é atingivel através de um plugin : 
 
![ ](https://bitbucket.org/mei-isep/edom-19-20-atb-14/src/master/project-research-topic/imagens/EclipsePlugin.PNG)

Uma das principais funções do componente core do Papyrus é permitir a colaboração de diferentes editores independentemente da sua tecnologia de implementação. 

#### 2.2.1 Application  engineering - Papyrus for Robotics

O Papyrus for Robotics apresenta um *modeling front-end*  que está em conformidade com os princípios fundamentais da RobMoSys de separação de papéis e preocupações. Ele fornece pontos de vista de arquitetura personalizados, diagram notations e visualizações de propriedades, incluindo (mas não limitados a) aqueles para a definição de componentes de software, serviços, representação da arquitetura do sistema e políticas de coordenação e configuração.

## 3. Exemplo de utilização de Papyrus

As seguintes imagens foram retiradas de um projeto de pesquisa, realizado por vários software designers, e estas representam a utilização de papyrus. 

![ ](https://bitbucket.org/mei-isep/edom-19-20-atb-14/src/master/project-research-topic/imagens/figura%201.PNG)

![ ](https://bitbucket.org/mei-isep/edom-19-20-atb-14/src/master/project-research-topic/imagens/figura2.PNG)

O exemplo consegue ilustrar bastante bem o objetivo da implementação de Papyrus: 99.9% da especificação sem impor qualquer tipo de restrições ou suposições. Isto é o principal objetivo do papyrus, visto que a maioria das ferramentas usadas não conseguem suportar UML2 viso que possuem restrições e estas estão *hard-coded*. 

### 3.1. Utilização do Papyrus - Papyrus for Robotics

Após a inicialização do Papyrus conseguimos ver a sua forma de atuação nas seguintes imagens: 
 ![ Papyrus View](https://bitbucket.org/mei-isep/edom-19-20-atb-14/src/master/project-research-topic/imagens/PapyrusROBView.PNG)
A imagem a cima ilustra a "vista" a quando do uso de papyrus. - Papyrus View. Ao que as imagens seguintes mostram a Palette, que é o local onde tem as ações necessarias para a construção dos diagramas ou modelos, e o model explorer.

![ Palette](https://bitbucket.org/mei-isep/edom-19-20-atb-14/src/master/project-research-topic/imagens/Palette.PNG)

![ Model Explorer](https://bitbucket.org/mei-isep/edom-19-20-atb-14/src/master/project-research-topic/imagens/ModelExplorer.PNG)

### 3.2 Alternativas

Uma das alternativas para o Papyrus é o Modelio ([https://alternativeto.net/software/modelio-open/](https://alternativeto.net/software/modelio-open/)). O Modelio é, tal como o papyrus, open-source. É uma modeling tool que suporta UML, BPMN, entre outras. Permite também desenhar todos os UML2 diagramas, assim como gerar codigo através dos mesmos.

## 4. Bibliografia

- https://www.eclipse.org/papyrus/
- [https://wiki.eclipse.org/Papyrus](https://wiki.eclipse.org/Papyrus)
- [https://en.wikipedia.org/wiki/Domain_engineering](https://en.wikipedia.org/wiki/Domain_engineering)
- [https://www.researchgate.net/publication/230561386_Papyrus_A_UML2_Tool_for_Domain-Specific_Language_Modeling_Model-Based_Engineering_of_Embedded_Real-Time_Systems/link/00b7d52d84bc829553000000/download](https://www.researchgate.net/publication/230561386_Papyrus_A_UML2_Tool_for_Domain-Specific_Language_Modeling_Model-Based_Engineering_of_Embedded_Real-Time_Systems/link/00b7d52d84bc829553000000/download)
- [https://www.unamur.be/en/precise/research-fields/mde](https://www.unamur.be/en/precise/research-fields/mde)
- [https://robmosys.eu/wiki/baseline:environment_tools:papyrus4robotics](https://robmosys.eu/wiki/baseline:environment_tools:papyrus4robotics)
- [https://alternativeto.net/software/papyrus-uml/?license=opensource&platform=eclipse&sort=recentlikes](https://alternativeto.net/software/papyrus-uml/?license=opensource&platform=eclipse&sort=recentlikes)

