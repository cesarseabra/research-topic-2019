# XATKIT
----------

## Introdução

De forma a consolidar e a compreender os conceitos lecionados nas aulas teoricas da disciplina Engenharia de Dominio(EDOM), foi nos fornecidos diversos topicos de pesquisa de forma a realizar-mos uma apresentação sobre o topico escolhido, Xatkit.

De uma forma geral, O Xatkit é uma plataforma genérica e extensível para o desenvolvimento de assistentes digitais. 
Um assistente digital é um tipo de bot projetado para interagir com pessoas de forma conversacional, com base em sua programação.


Em contexto de Engenharia de dominio, O Xatkit é: 
Uma plataforma de desenvolvimento de chatbot model-based e low-code, onde os chatbots são definidos usando algumas DSLs externas e através de um runtime engine este implanta e executa os bots nas plataformas desejadas.

O Xatkit também contém "textual DSLs (in XText), a chatbot metamodel (in EMF), a PIM vs PSM distinction, platform and event/execution models,… all following a pure 100% model-based approach."



## Suporte multiplataforma

Algumas plataformas são capazes de enviar eventos / mensagens para os bots (por exemplo, Slack informando que alguém está a conversar com o bot em um algum canal) enquanto outros estão a escutar as instruções fornecidas pelos bots (por exemplo, a plataforma GitHub pode esparar que seja criado um novo relatório de erro com base num chat com um usuário). Algumas plataformas estão prontas para a comunicação bidirecional (por exemplo, podemos receber mensagens do Slack, e também enviar uma quando algo acontecer noutra plataforma).

Por exemplo:

- Slack
- Twitter
- React
- Discord
- Giphy
- GitHub
- UML

## Bots inteligentes

Reconhecimento fácil de intents com base em aprendizagem Machine Learning, graças à integração com o DialogFlow (ou outro provedor de reconhecimento de linguagens).

## Definição de Bot a partir de texto

Melhor produtividade graças ao conjunto de idiomas textuais para definir os fluxos de conversa e respostas (avançadas) aos intents dos usuários com base nos serviços fornecidos pelas plataformas suportadas..

## Grátis

O Xatkit é completamente grátis. O download está disponivel (e pode ser modificado) a partir do repositório GitHub. 

## Visão global

A figura abaixo mostra a visão geral da estrutura do Xatkit. O designer especifica o chatbot em construção usando o Xatkit Modeling Language, que define três pacotes:

![xatkit-overview](xatkit-overview.png)


** Intent Package ** que permite descrever as intenções do usuário usando frases de treino, extração de informações contextuais e condições.

![intent-editor](intent-editor.png)

A primeira linha define o nome da biblioteca dos *intents*. O nome da biblioteca é usado para identificar a biblioteca e fornecer informações úteis de preenchimento automático nos outros editores.

O restante do ficheiro é composto por dois *intents* que representam as intenções do usuário às quais o GreetingsBot responde. Cada intent contém um conjunto de entradas que são exemplos de mensagens do usuário correspondentes à intenção. Neste exemplos o bot é definido em inglês, mas idiomas adicionais também são suportados.


**Platform Package** que permite especificar as possíveis ações disponíveis nas plataformas de destino, incluindo aquelas existentes apenas em ambientes específicos, por exemplo enviar uma mensagem para um canal Slack, criar uma issue no Github etc.


**Execution Package** que permite vincular intenções do usuário a ações como parte da definição de comportamento do chatbot, por exemplo enviar uma mensagem de boas-vindas ao utilizador quando ele pretende iniciar a conversa.

![execution-editor](execution-editor.png)

Os modelos de execução contêm 3 seções:


- um conjunto de *imports* de bibliotecas e plataformas que definem quais são as bibliotecas e plataformas de *intents* usadas pelo bot.


- pelo menos uma cláusula de provedor de uso que especifique quais são os *inputs* que o bot ouvirá.


- um conjunto de regras de execução que vincula as intenções do usuário às ações (as regras de execução também suportam construções adicionais que não são abordadas neste exemplo).

## Contexto com EDOM

O cenário de utilização desta ferramenta é a criação de chatbots para as diversas plataformas.

O xatkit usa metamodelos para modelar as diversas camadas do bot, nomeadamente os:

- **Intent package** :

![intent-modeling](intent_modeling.png)

- **platform package** :

![platform-modeling](platform-modeling.png)

- **execution package** :

![exectuion-modeling](execution-modeling.png)

O xatkit fornece um conjunto de DSLs que permitem especificar nao só a conversão logica de intents para executions, como também, a integração com aplicações de terceiros.

Possivelmente existe uma transformaçao Model-to-Model que permite ajustar o chatbot ás diversas plataformas.

Existe também uma parte implicita de geração de código que transforma a informação fornecida(intents, executions) no chatbot em si.

## Referências

- https://xatkit.com
- https://github.com/xatkit-bot-platform
- https://modeling-languages.com/multi-platform-chatbot-modeling-deployment-jarvis/
- https://modeling-languages.com/slack-uml-modeling/
- https://modeling-languages.com/building-chatbots-use-case-modeling-course/