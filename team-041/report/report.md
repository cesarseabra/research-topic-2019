# Project Research Report- Sirius

#### Group Elements:

- César Seabra - 1141399

- Guillermo Pedraz - 1140352

- Vasco Lusitano - 1140338

  

## Introduction

This report falls within the scope of the EDOM course and aims to develop a research report related to Model Driven Engineering (MDE) application tools and their integration into Eclipse. Regarding our group, the selected tool was Eclipse Sirius.

## Tool Presentation and Applications

Sirius is a tool resultant from the collaboration between Thales [1] and Obeo [2] in 2007.  The Thales goal was to obtain a generic workbench for model-based architecture engineering that could be easily tailored to fit the specific needs of each Thales project. This tool had to provide ergonomic facilities and support very large models. 

Thus, Sirius is an modeling based tool that allows the user to create customized graphical modeling tools and is purely integrated with Eclipse since it leverages its modeling tools - EMF (Eclipse Modeling Framework) for the model management and GMF (Graphical Modeling Framework) for the graphical representation [3]. All shape characteristics and shapes itself can be easily configured with minimal technical knowledge and, once the modeling is completed, it can be deployed as a Eclipse plugin.

This tool is particularly useful for users that have a predefined DSL (Domain Specific Language) and need graphical representations to better elaborate and analyze a system and improve the communication with other team members or customers [3]. 

Sirius is used in the most diverse fields from insurance and administration to transportation and communication. By exploring the page of this Tool, several workbenches for the most diverse areas can be found, between the most relevant are:

- **Ecore Tools** [4]  - Allows the construction of all the Ecore classical constructions. Represents the tool used in this course in the first increment;
- **UML Designer** [5] - Allows the creation of UML 2.5 models such as Use cases, components, class diagrams, amongst others. In Figure 1, it is visible an example of a Use Case diagram created in this workbench;
- **AdvoCATE** [6] - Created by NASA, used to specify Safety / Assurance cases on flight operations.

| ![usecase-model-example](img/uml-example.png) |
| :-------------------------------------------: |
|   *Figure 1 - Example Use Case Diagram* [7]   |


## Sirius in MDE - Domain Engeneering and Application Engeneering

In this section, an example on the usage of the Sirius tool to graphically manipulate a DSL to handle a series of instructions and actions ran by a robot (a choreography), such as going forward, rotating, grabbing and releasing. 

For this example, we will use Ecore language to define a model that represents such choreographies and then use Sirius to create a graphical editor that allows the user to visualize and edit the model. In figure 2, is presented a visual representation of the process and how it conforms with both the Sirius and EMF tools.

| ![sirius-principles](img/sirius-principles.png) |
| :---------------------------------------------: |
|   *Figure 2 - Sirius modeling principles* [8]   |

The instances created are then transformed into Java code (model-to-text) using Acceleo. This last part is optional and external to Sirius.

So, firstly we create the metamodel for the choreographies by creating a new Ecore Modeling Project.

| ![domain-model](img/domain-model.png)  |
| :------------------------------------: |
| *Figure 3 - Choreography Domain Model* |

In Figure 3, is represented the metamodel for the choreographies. A choreography can be composed of various sub-choreographies and contains actions that, in turn, can be of the type - **GoForward**, **Rotate**, **Grab** or **Release**.

Since the model created is based on the Ecore language, it allows OCL validations and the creation of instances (*.xmi and *.mindstorm) and posterior validations of these instances with the OCL rules declared.

After creating the model and generating the code, we import the new icons that will be used to graphically represent each object in the model, in the Sirius graphical editor.

| ![instance](img/instance.png)  |
| :------------------------------------: |
| *Figure 4 - Created Instance* |

In the Figure 4 presented above, is represented a created instance of the Choreography model. It's noticeable that the rotation icons change accordingly to the degrees signal (positive or negative).

Now we pass to the creation of the Sirius modeling tool (workbench) by creating a new **Viewpoint Specification Project** and creating a new diagram description to contain all the **Choreographies**.

For this part, we create  a new diagram to represent our instances. By doing this, we need to create our nodes, relationships and style customizations. To query the elements of the model, Sirius uses AQL (Acceleo Query Language) to navigate and query the EMF model. In the next picture, it's represented the configuration of the new view.

| ![1st-conf](img/1st-conf.png)  |
| :------------------------------------: |
| *Figure 5 - Sirius Model Design* |

On the creation phase to the end-user, the diagram will be represented as shown in the Figure 6.

| ![1st-result](img/1st-result.png)  |
| :------------------------------------: |
| *Figure 6 - Model Result* |

On the second part, we set the sub-choreographies to be represented as containers with their instructions and making so that, upon double-clicking the sub-choreography, it opens the correspondent diagram directly. In this phase, also the palette creation tools are provided to add the several instructions dynamically.

| ![final](img/graphical-dsl.png)  |
| :------------------------------------: |
| *Figure 7 - Final Graphical DSL* |

In Figure 7, we can see the final aspect of the created graphical DSL (our workbench), complete with with all the tools needed to create new models of the metamodel created initially to our problem.

| ![validations](img/validation-rule-final.png)  |
| :------------------------------------: |
| *Figure 8 - Model Validations* |

In this part, there where also validations performed in order to warn the end-user of the constraints applied to the values. For example, a sub-choreography must have an unique name, thus this validation is created as presented in A and, when the user enters an already used Sub-Choreography name an error message is presented - B.

It's also important to refer that Sirius allows the writing of unit tests although this implementation still runs with JUnit3. This implementation wasn't further explored.

Finally, to perform the model-to-text transformation, as previously mentioned, Acceleo was used. By creating a new Acceleo Project and importing the Ecore model created initially we where able to perform transformations in the instances created with Sirius to Java code. In Figure 9, is present , on the left the the Acceleo transformation code and on the right one of the generated Java classes.

| ![acceleo](img/acceleo.png)  |
| :------------------------------------: |
| *Figure 9 - Acceleo: Model-to-Text Transformation* |

> It's important to reference that even though no model-to-model transformations were implemented, these could be easily done with a tool such as ATL (Atlas Transformation Language). Since when creating our Sirius graphical DSL, we had to create an Ecore metamodel first, this would allow us to define our entry metamodel. We could achieve a model to model transformation by, for example, creating a new  metamodel that would allow more instructions (like Climbing or Crouching) for a more complex robot and then applying the ATL transformation between models.

## Sirius Tools Alternatives

As said in the previous point, since the metamodel is created using EMF Ecore, it supports OCL validations also, as it happens in Xtext, also in Sirius it's possible do define end-user validations to the model so that it conforms to the expected behavior on variables changes trough AQL language. Their biggest difference is the type of modeling they use, Xtext is textual and Sirius is graphical. Textual modeling is significantly better for analyzability coverage and modifiability, although graphical modeling is more interactive.

For model to model transformations, as mentioned before, ATL is in the scope but other tools can be use such as Epsilon [9] or Viatra [10]. ATL supports in-place transformations through its refining mode (meaning  it is simple to define refining rules and elements that are not impacted by those rules), while ETL or Epsilon supports interactive model transformations (people can make decisions that are hard to do automatically within a model transformation).

For the model to text transformations (code generation), we have used Acceleo. Nevertheless, Epsilon could also be used for this task or even XPand [11]. The main difference between Acceleo and Xpand comes from the fact that Acceleo is based on the standards MOFM2T and OCL from the OMG and the tooling.


## References

[1] “Thales Group.” [Online]. Available: https://www.thalesgroup.com/en. [Accessed: 24-Nov-2019].

[2] “Obeo.” [Online]. Available: https://www.obeo.fr/en/. [Accessed: 24-Nov-2019].

[3] “What is Sirius? | The Eclipse Foundation.” [Online]. Available: https://www.eclipse.org/community/eclipse_newsletter/2013/november/article1.php. [Accessed: 24-Nov-2019].

[4] “EcoreTools - Graphical Modeling for Ecore.” [Online]. Available: https://www.eclipse.org/ecoretools/overview.html. [Accessed: 24-Nov-2019].

[5] “UML Designer.” [Online]. Available: http://www.umldesigner.org/. [Accessed: 24-Nov-2019].

[6] “Robust Software Engineering - AdvoCATE.” [Online]. Available: https://ti.arc.nasa.gov/tech/rse/research/advocate/. [Accessed: 24-Nov-2019].

[7] “Sirius - Gallery.” [Online]. Available: https://www.eclipse.org/sirius/gallery.html. [Accessed: 24-Nov-2019].

[8] “Sirius Tutorials - Mindstorms.” [Online]. Available: https://wiki.eclipse.org/Sirius/Tutorials/Mindstorms/Introduction. [Accessed: 24-Nov-2019].

[9] “Epsilon.” [Online]. Available: https://www.eclipse.org/epsilon/. [Accessed: 25-Nov-2019].

[10] “Viatra.” [Online]. Available: https://www.eclipse.org/viatra/. [Accessed: 25-Nov-2019].

[11] “Xpand.” [Online]. Available: https://wiki.eclipse.org/Xpand. [Accessed: 25-Nov-2019].