/**
 */
package pt.isep.edom.mindstorms.impl;

import org.eclipse.emf.ecore.EClass;

import pt.isep.edom.mindstorms.MindstormsPackage;
import pt.isep.edom.mindstorms.Release;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Release</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ReleaseImpl extends ActionImpl implements Release {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ReleaseImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MindstormsPackage.Literals.RELEASE;
	}

} //ReleaseImpl
