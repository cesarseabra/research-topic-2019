/**
 */
package pt.isep.edom.mindstorms.impl;

import org.eclipse.emf.ecore.EClass;

import pt.isep.edom.mindstorms.Grab;
import pt.isep.edom.mindstorms.MindstormsPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Grab</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class GrabImpl extends ActionImpl implements Grab {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected GrabImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MindstormsPackage.Literals.GRAB;
	}

} //GrabImpl
